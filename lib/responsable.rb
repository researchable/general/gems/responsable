# frozen_string_literal: true

require_relative 'responsable/version'
require 'dry-configurable'

# The {#Responsable} module provides a consistent way to render json responses across all Researchable's services
module Responsable
  extend Dry::Configurable
  setting :serializer, default: proc { |resource| resource.as_json }

  # Method to render a resource in a consistent way. If the resource is not valid it will render a validation error
  # @param resource the resource to render
  def render_resource(resource)
    if resource.errors.empty?
      render json: resource
    else
      validation_error(resource.errors)
    end
  end

  # Method to render validation errors in a consistent way
  # @param resource_errors the errors to render
  def validation_error(resource_errors)
    render json: {
      errors: [
        {
          status: '400',
          title: 'Bad Request',
          detail: resource_errors,
          code: '100'
        }
      ]
    }, status: :bad_request
  end

  # Method to render access denied errors in a consistent way
  # @param resource_errors the errors to render
  def access_denied(resource_errors)
    render json: {
      errors: [
        {
          status: '403',
          title: 'Access Denied',
          detail: resource_errors,
          code: '100'
        }
      ]
    }, status: :forbidden
  end

  # Method to render not found errors in a consistent way
  # @param resource_errors the errors to render
  def not_found(resource_errors)
    render json: {
      errors: [
        {
          status: '404',
          title: 'Not Found',
          detail: resource_errors,
          code: '100'
        }
      ]
    }, status: :not_found
  end

  # Method to render created status in a consistent way
  # @param resource the resource to render
  def created(resource = nil)
    render json: {
      result: [
        {
          status: '201',
          title: 'created',
          detail: 'resource created',
          code: '100',
          instance: Responsable.config.serializer.call(resource)
        }
      ]
    }, status: :created
  end

  # Method to render success status in a consistent way
  # @param resource the resource to render
  def render_success_resource(resource)
    render json: {
      result: [
        {
          status: '200',
          title: 'Success',
          detail: resource,
          code: '100'
        }
      ]
    }, status: :ok
  end

  # Method to render destroyed status in a consistent way
  def destroyed
    render json: {
      result: [
        {
          status: '200',
          title: 'destroyed',
          detail: 'resource destroyed',
          code: '100'
        }
      ]
    }, status: :ok
  end

  # Method to render unprocessable entity errors in a consistent way
  # @param resource_errors the errors to render
  def unprocessable_entity(resource_errors)
    render json: {
      errors: [
        {
          status: '422',
          title: 'unprocessable',
          detail: resource_errors,
          code: '100'
        }
      ]
    }, status: :unprocessable_entity
  end

  # Method to render no content in a consistent way
  def no_content
    render json: {
      result: [
        {
          status: '204',
          title: 'no content',
          detail: 'no content provided',
          code: '100'
        }
      ]
    }, status:  :no_content
  end

  # Method to render not implemented in a consistent way
  # @param detail the description of the functionality that is not supported
  def not_implemented(detail)
    render json: {
      result: [
        {
          status: '501',
          title: 'Not Implemented',
          detail: detail,
          code: '100'
        }
      ]
    }, status: :not_implemented
  end

  # Method to render paginated resources in a consistent way
  # @param paginated_query the query that has been paginated with Kaminari
  # @param serialized_data the serialized resource (should not be a hash `{data: ...}` to prevent double nesting)
  def render_page(paginated_query, serialized_data)
    if paginated_query.respond_to? :current_page
      render json: {
        data: serialized_data,
        page: {
          current_page: paginated_query.current_page,
          next_page: paginated_query.next_page,
          prev_page: paginated_query.prev_page,
          total_count: paginated_query.total_count,
          total_pages: paginated_query.total_pages,
          has_next: !paginated_query.last_page? && paginated_query.size.positive?
        }
      }
    else
      not_implemented('a pagination gem that implements #current_page is needed')
    end
  end
end
