# frozen_string_literal: true

require_relative 'lib/responsable/version'

Gem::Specification.new do |spec|
  spec.name = 'responsable'
  spec.version = Responsable::VERSION
  spec.authors = ['Researchable']
  spec.email = ['info@researchable.nl']

  spec.summary = "Researchable's gem to standardize http REST responses"
  spec.description = "Researchable's gem to standardize http REST responses"
  spec.homepage = "https://gitlab.com/researchable/general/gems/responsable/-/blob/v#{Responsable::VERSION}/README.md"
  spec.license = 'MIT'
  spec.required_ruby_version = '>= 3.0'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/researchable/general/gems/responsable'
  spec.metadata['changelog_uri'] = "https://gitlab.com/researchable/general/gems/responsable/-/blob/v#{Responsable::VERSION}/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (File.expand_path(f) == __FILE__) || f.start_with?(*%w[bin/ test/ spec/ features/ .git .circleci appveyor])
    end
  end
  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  # Register gem dependencies here
  spec.add_dependency 'dry-configurable', '~> 0.16'

  # For more information and examples about making a new gem, check out our
  # guide at: https://bundler.io/guides/creating_gem.html

  # We have to disable MFA for pushing gems because otherwise we can't automate
  # this process as we have the OTP code in 1pw somewhere.
  # rubocop:disable Gemspec/RequireMFA
  spec.metadata = {
    'rubygems_mfa_required' => 'false'
  }
  # rubocop:enable Gemspec/RequireMFA
end
