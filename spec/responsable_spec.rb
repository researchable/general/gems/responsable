# frozen_string_literal: true

RSpec.describe Responsable do
  it 'has a version number' do
    expect(Responsable::VERSION).not_to be nil
  end
end
