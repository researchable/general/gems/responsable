# [1.4.0](https://gitlab.com/researchable/general/gems/responsable/compare/v1.3.0...v1.4.0) (2023-12-6)


### Features

* disable MFA requirement because otherwise it breaks the automated pipeline ([0c3bf8d](https://gitlab.com/researchable/general/gems/responsable/commit/0c3bf8dbb67cb23b9913063b5dc484d1dd46f74f))
* rubocop + version bump ([e179223](https://gitlab.com/researchable/general/gems/responsable/commit/e17922334afd7dc3a882a38118a5379899a03c36))

# [1.3.0](https://gitlab.com/researchable/general/gems/responsable/compare/v1.2.0...v1.3.0) (2023-12-6)


### Features

* Bump version number again ([e9ca8fb](https://gitlab.com/researchable/general/gems/responsable/commit/e9ca8fb70fca220898fba071f6dd9288e7c4bec4))

# [1.2.0](https://gitlab.com/researchable/general/gems/responsable/compare/v1.1.0...v1.2.0) (2023-12-6)


### Features

* version bump ([15f9cb6](https://gitlab.com/researchable/general/gems/responsable/commit/15f9cb6a5579cbfecdf200946209248d87a4cb6e))

# [1.1.0](https://gitlab.com/researchable/general/gems/responsable/compare/v1.0.0...v1.1.0) (2023-12-5)


### Features

* add #not_implemented ([deda9b2](https://gitlab.com/researchable/general/gems/responsable/commit/deda9b22a196372d5da474ad817311ddb34f5ee6))
* add #render_page method ([a666152](https://gitlab.com/researchable/general/gems/responsable/commit/a66615252855d554558c56640ccc924068a3e3bb))

# 1.0.0 (2023-04-20)


### Features

* first implementation ([3a3635c](https://gitlab.com/researchable/general/gems/responsable/commit/3a3635c3565d266ae00b324ec7fa97266fc01f93))
* make serializer configurable ([29600cf](https://gitlab.com/researchable/general/gems/responsable/commit/29600cfecb942c8c9d6ecd5393845b45e7ef6abf))
