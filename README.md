# Responsable

Responsable is a gem to standardize response objects and messages across all Researchable's services.

## Installation


Add this to your Gemfile:
```ruby
gem 'responsable', '~> 1.0'
```

Then execute:
```bash
$ bundle install
```

## Usage

In your application controller, include the `Responsable` module:

```ruby
class ApplicationController < ActionController::Base
  include Responsable
  
  # rest of the controller
end
```

Then, in your controller actions, you can use any of the available response methods in `lib/responsable.rb`.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
